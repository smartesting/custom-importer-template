import {
    v4 as uuidv4
} from 'uuid';

export const convertor = (file) => {
    let agilkiaJson = JSON.parse(file);
    let traces = [];
    for (let i = 0; i < agilkiaJson.traces.length; i++) {
        const agilkiaTrace = agilkiaJson.traces[i];
        traces.push({
            id: uuidv4(),
            name: 'trace_' + (i + 1),
            info: metadataToInfo(agilkiaTrace.meta_data),
            steps: agilkiaTrace.events.map(event => {
                return {
                    info: [],
                    values: paramsToValues(event.inputs).concat(paramsToValues(event.outputs)),
                    events: [{
                        info: metadataToInfo(event.meta_data),
                        action: event.action,
                        inputs: paramsToValues(event.inputs),
                        outputs: paramsToValues(event.outputs)
                    }]
                }
            }),
        })
    }
    return traces;
}

function metadataToInfo(meta_data) {
    let info = [];
    for (var key in meta_data) {
        info.push({
            key: key,
            value: createValue(meta_data[key])
        });
    }
    return info;
}

function createValue(value) {
    let res = '' + JSON.stringify(value);
    if (res.startsWith('"') && res.endsWith('"')) {
        return res.substring(1, res.length - 1);
    }
    return res;
}

function paramsToValues(params) {
    let data = [];
    for (var name in params) {
        data.push({
            field: name,
            value: createValue(params[name])
        });
    }
    return data;
}