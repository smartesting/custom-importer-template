import React, { useState } from "react";
import Files from 'react-files';
import { EventParameter } from "./components/EventParameter";
import { convertor } from "./convertor";

export const App = ({ callback }) => {
  const [traces, setTraces] = useState(null);
  const [traceSelected, setTraceSelected] = useState(null);

  const loadFile = (files) => {
    const reader = new FileReader();
    reader.onload = e => {
      let traces;

      try {
        traces = convertor(e.target.result);
      } catch (e) {
        alert("Une erreur s'est produite lors de la conversion : " + e);
        return;
      }

      if (callback) {
        callback(traces);
      } else {
        if (traces.length === 0) {
          alert("Aucune trace à importer");
        } else {
          setTraceSelected(traces[0]);
          setTraces(traces);
        }
      }
    }
    reader.onerror = function () {
      alert("Une erreur s'est produite lors de la lecture du fichier !");
    };
    reader.readAsText(files[0], "UTF-8");


  };

  return traces === null ?
    <Files className="import-tile"
      onChange={loadFile}
      multiple={false}
      onError={(error) => alert("Une erreur s'est produite : " + error.message)}
      maxFileSize={1024 * 1024 * 200} // 200Mb max
      minFileSize={0}
      clickable>
      <div className="import-tile-content">
        Déposer ou choisir un fichier <b>Custom</b>
        <div className="upload-symbol" />
      </div>
    </Files>
    :
    <div className="app">
      <div className="button-wrapper">
        <div className="back-button" onClick={() => setTraces(null)}>Retour</div>
      </div>
      <div className="content-wrapper">
        <div className="traces-list">
          {traces.map((trace, traceIndex) => <div key={`trace-${traceIndex}`}
            className={"trace-name" + (traceSelected === trace ? " selected" : "")}
            onClick={() => setTraceSelected(trace)}>{trace.name}</div>)}
        </div>
        <div className="trace-viewer">
          {traceSelected.steps.map(step => step.events.map((event, eventIndex) => {
            return <div key={`step-event-${eventIndex}`} className="step-event">
              <div className="step-event-content">
                <div className="step-event-part">
                  <div className="step-event-part-title">Évènement</div>
                  <div className="event-action">
                    {event.action}
                  </div>
                </div>
                <div className="step-event-part">
                  <div className="step-event-part-title">Entrées</div>
                  <div className="event-inputs">
                    {
                      event.info.length === 0 ?
                        "-"
                        :
                        event.inputs.map((input, inputIndex) => <EventParameter key={`event-input-${inputIndex}`} parameterKey={input.field} parameterValue={input.value} />)
                    }
                  </div>
                </div>
                <div className="step-event-part">
                  <div className="step-event-part-title">Sorties</div>
                  <div className="event-outputs">
                    {
                      event.outputs.length === 0 ?
                        "-"
                        :
                        event.outputs.map((output, outputIndex) => <EventParameter key={`event-output-${outputIndex}`} parameterKey={output.field} parameterValue={output.value} />)
                    }
                  </div>
                </div>
                <div className="step-event-part">
                  <div className="step-event-part-title">Infos</div>
                  <div className="event-info">
                    {
                      event.info.length === 0 ?
                        "-"
                        :
                        event.info.map((info, infoIndex) => <EventParameter key={`event-info-${infoIndex}`} parameterKey={info.key} parameterValue={info.value} />)
                    }
                  </div>
                </div>
              </div>
            </div>
          }))}
        </div>
      </div>
    </div>

};
