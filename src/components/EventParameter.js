import React from "react";

export const EventParameter = ({ parameterKey, parameterValue }) => {
    const color = generatePastelColorFrom(parameterKey);
    const colorWithReducedOpacity = color.replace(/.$/, ",70%)");
    return <div style={{"border": "1px solid " + colorWithReducedOpacity, "color": colorWithReducedOpacity}} className="event-parameter"><i>{parameterKey}</i> : <b>{parameterValue}</b></div>;
};

function generatePastelColorFrom(string) {
    let hash = 0, i, chr;
    if (string.length === 0) return hash;
    for (i = 0; i < string.length; i++) {
        chr = string.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0;
    }
    hash = parseFloat('0.' + Math.abs(hash))
    let cssHSL = "hsl(" + (120 + 240 * hash) + ',' +
        (25 + 70 * hash) + '%,' +
        (55 + 10 * hash) + '%)';
    return cssHSL;
}