# Comment créer un import personnalisé pour Gravity ? #

Vous êtes sur un dépôt qui a pour but d'être cloné afin de vous aider à créer un import personnalisé. 
Suivez les étapes suivantes pour réaliser sa mise en place.

### Étape #1 : récupérez le projet  ###

Commencez par ouvrir un terminal puis entrez la commande suivante :
```sh
git clone https://bitbucket.org/smartesting/custom-importer-template.git
```
⚠ Git est requis pour utiliser cette commande. Vous pouvez l'installer [ici](https://git-scm.com/downloads).

### Étape #2 : implémentez et testez votre import  ###

Après avoir récupéré le projet, rendez-vous dans son dossier puis ouvrez un terminal et entrez les commandes suivantes :
```sh
npm i
npm start
```
Une page web devrait s'ouvrir à l'adresse http://localhost:9090/. Elle contient le visuel de l'import à implémenter mais celui-ci n'est pas fonctionnel. 

C'est à vous d'implémenter le fichier **convertor.js** pour que le fichier **file** passé en paramètre de la méthode **convertor** soit transformé en **traces Gravity**. 

Vous pouvez directement tester votre import sur la page web ouverte après enregistrement de vos modifications.

### Étape #3 : inspirez-vous d'exemples  ###

Si vous ne voyez pas comment vous y prendre pour modifier le fichier **convertor.js**, consultez les autres fichiers **convertor.js** présents dans le dossier **examples**. Cela vous aidera peut-être à démarrer !

### Étape #4 : utilisez votre import dans Gravity  ###

Une fois que votre import est fonctionnel, ouvrez un terminal et entrez la commande suivante :
```sh
npm run build
```
Ensuite, récupérez le fichier **main.js** nouvellement généré dans le dossier **dist** puis hébergez-le sur un service web d'hébergement tel que Github ou Bitbucket. 

Enfin, consultez la version **raw** du fichier sur le site d'hébergement puis recopiez son l'URL sur Gravity afin d'initialiser l'import.

### En cas de problème  ###
Si vous rencontrez des problèmes ou que vous avez des questions, n'hésitez pas à nous contacter, soit via le chat instantané de Gravity, soit par [mail](mailto:gravity@smartesting.com).